FROM python:3.9.7

MAINTAINER user

WORKDIR app

COPY ./requirements.txt /app/requirements.txt

RUN pip install -r requirements.txt

COPY . .

ENV FLASK_APP=main.py

ENV FLASK_ENV=development

EXPOSE 5000

CMD ["flask", "run", "--host", "0.0.0.0"]
