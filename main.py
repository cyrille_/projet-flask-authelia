from flask import Flask
app = Flask (__name__)
csrf = CSRFProtect()
csrf.init_app(app) # Compliant
@app.route('/', methods=['POST'])
def home():
   return "hello world!"
